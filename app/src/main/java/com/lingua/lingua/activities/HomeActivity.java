package com.lingua.lingua.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.lingua.lingua.R;
import com.lingua.lingua.fragments.HomeFragment;
import com.lingua.lingua.fragments.ProfileFragment;
import com.lingua.lingua.fragments.SearchFragment;
import com.lingua.lingua.fragments.adapters.FragmentAdapter;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;

/*
 * Class : .
 * Created by mzusman - morzusman@gmail.com on 5/2/16.
 */
public class HomeActivity extends AppCompatActivity {
    ViewPager viewPager;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new SearchFragment());
        fragments.add(new ProfileFragment());

        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), fragments);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        if (viewPager != null) {
            viewPager.setAdapter(fragmentAdapter);
        }
        SmartTabLayout smartTabLayout = (SmartTabLayout) findViewById(R.id.viewpagertab);
        if (smartTabLayout != null) {
            smartTabLayout.setViewPager(viewPager);
        }


    }
}
