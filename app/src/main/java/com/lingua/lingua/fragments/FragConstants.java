package com.lingua.lingua.fragments;

/*
 * Class : .
 * Created by mzusman - morzusman@gmail.com on 5/2/16.
 */
public class FragConstants {
    private FragConstants(){
    }
    public static final String PROFILE_TAG = "PROFILE";
    public static final String SEARCH_TAG = "SEARCH";
    public static final String EDIT_PROFILE_TAG = "EDIT";
//    public static final String PROFILE_TAG = "PROFILE";

}

