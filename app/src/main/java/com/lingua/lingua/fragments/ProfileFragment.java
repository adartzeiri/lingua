package com.lingua.lingua.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lingua.lingua.R;

/*
 * Class : .
 * Created by mzusman - morzusman@gmail.com on 5/2/16.
 */
public class ProfileFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        TextView textView = (TextView) v.findViewById(R.id.profile_tv);
        textView.setText("Profile");


        return v;

    }
}
